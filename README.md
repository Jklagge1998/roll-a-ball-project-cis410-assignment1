# Roll a Ball Project #

This was the first assignment for my introduction to video game programming class. In the Roll a Ball project the player takes control a ball where they roll on a plane collecting pickups (Yellow rotating squares). Once they collect all 12 they win.

### Summary ###

* As instructed I followed the roll a ball project tutorial from unity.
* To see this tutorial click the following link [https://learn.unity.com/project/roll-a-ball-tutorial](https://learn.unity.com/project/roll-a-ball-tutorial)
* I then implemented a double jump ability for the player. If the player is on the ground they can jump by hitting the space bar, and while in the air they can hit the space bar again to jump once more. After that the player can't jump until they hit the ground.

### Skills Used or Learned ###
* Creating basic objects in the Scene Hierachy.
* Basic knowledge and C# Scripting involving rigidbodies, User Interface (UI), User Input, and object collisions. 
* Creation of Materials and Prefabs.

### Notes ###

* Other than adding a double jump the project pretty much follows the the tutorial; except, I made the walls taller so the player couldn't fall out of the area of play.
* I build this for Windows and it ran fine. It should also run fine for other builds that support keyboards.
* I personally pulled this repo onto my local device and added it to unity hub. When I loaded up the project everything seemed to be in order.
* The version of unity this project runs on is 2019.1.14f. It has not been built, ran, or tested for any other version of unity.

### Have any questions? ###

* Email me @ Jklagge@uoregon.edu
