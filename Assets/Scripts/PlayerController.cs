﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float jumpSpeed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;
    private bool onGround;
    private int jumpCount;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        onGround = true;
        jumpCount = 0;
    }
    // update is called before a frame is rendered
    // our game code will go here
    void Update()
    {
        if (onGround)
        {
            if (Input.GetKeyDown("space"))
            {
                rb.velocity = new Vector3(0.0f, jumpSpeed, 0.0f);
                onGround = false;
                jumpCount += 1;
            }
        }
        else if (jumpCount < 2)
        {
            if (Input.GetKeyDown("space"))
            {
                rb.velocity = new Vector3(0.0f, jumpSpeed, 0.0f);
                jumpCount += 1;
            }
        }
       
        
    }

    // Fixed Update is called before a physics calculations
    // our physics code will go here
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
       if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count += 1;
            SetCountText();
        }
    }
    private void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winText.text = "You Win!";
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            onGround = true;
            jumpCount = 0;
        }
    }
}
